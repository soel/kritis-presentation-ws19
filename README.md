Presentation for KRITIS, Winter Semester 2019.

Online viewable at [soel.gitlab.io/kritis-presentation-ws19](https://soel.gitlab.io/kritis-presentation-ws19) or downloadable from this very project page.

Made with [reveal.js](https://github.com/hakimel/reveal.js)!

